#!/usr/bin/env node

var fs = require('fs'),
    esprima = require('esprima'),
    estraverse = require('estraverse'),
    doctrine = require('doctrine');

function analyze(node) {
    var comment, data, params, missing;

    comment = node.leadingComments[0];
    if (comment.type !== 'Block') {
        return;
    }
    data = doctrine.parse(comment.value, {unwrap: true});

    params = [];
    data.tags.forEach(function (tag) {
        if (tag.title === 'param') {
            params.push(tag.name);
        }
    });

    missing = [];
    node.params.forEach(function (param) {
        if (params.indexOf(param.name) < 0) {
            missing.push(param.name);
        }
    });
    if (missing.length > 0) {
        console.error('In function', node.id.name, '(Line', node.loc.start.line + '):');
        missing.forEach(function (m) {
            console.error(' Parameter', m, 'is not documented.');
        });
        console.error();
    }
}

function verify(node) {
    switch (node.type) {
        case esprima.Syntax.FunctionDeclaration:
            if (node.leadingComments.length === 1) {
                analyze(node);
            }
            break;
        default:
            break;
    }
}

function check(filename) {
    var content, tree;
    try {
        content = fs.readFileSync(filename, 'utf-8');
        tree = esprima.parse(content, { attachComment: true, loc: true });
        estraverse.traverse(tree, { enter: verify });
    } catch (e) {
        console.error(e.toString());
        process.exit(1);
    }
}

if (process.argv.length === 2) {
    console.error('Usage: missing-doc.js filename');
    process.exit(1);
}
check(process.argv[2]);
