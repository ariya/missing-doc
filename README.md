This is an example of using [Esprima](http://esprima.org/) and [Doctrine](https://github.com/Constellation/doctrine/).

Run the following:

```
npm install
node missing-doc.js test/sample2.js
```

For more details, check the [blog post](http://ariya.ofilabs.com/2014/04/tracking-javascript-annotations.html).
